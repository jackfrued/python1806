"""fangtx URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from rest_framework.routers import DefaultRouter

from api.views import provinces, cities, login
from api.views import EstateViewSet, HouseTypeViewSet, HouseInfoViewSet, SearchViewSet

urlpatterns = [
    path('login/', login, name='login'),
    path('districts/', provinces, name='provinces'),
    path('districts/<int:distid>/', cities, name='cities'),
]

router = DefaultRouter()
router.register('estates', EstateViewSet)
router.register('housetypes', HouseTypeViewSet)
router.register('houseinfos', HouseInfoViewSet)
router.register('search', SearchViewSet, base_name='search')

# http://localhost:8000/api/search/?text=拎包

urlpatterns += router.urls

# GET /api/districts/
# GET /api/districts/{id}/

# GET /api/estates/
# 请求参数:
# - page 数值 否 页码 默认值1
# - size 数值 否 页码大小 默认值10（最大不超过50）
# - name 字符串 否 楼盘名字 支持前缀模糊查询
# - district 数值 否 楼盘区域

# GET /api/housetypes/
# GET /api/housetypes/{id}/
# POST /api/housetypes/
# PUT /api/housetypes/{id}/
# PATCH /api/housetyes/{id}/
# DELETE /api/housetypes/{id}/

# GET /api/houseinfos/
# 请求参数:
# - title 字符串 否 标题 支持模糊查询
# - min_price 数值 否 价格下限
# - max_price 数值 否 价格上限
# GET /api/houseinfos/{id}/
