"""
搜索引擎索引类
"""
from haystack import indexes

from common.models import HouseInfo


class HouseInfoIndex(indexes.SearchIndex, indexes.Indexable):
    """房源全文检索索引"""

    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return HouseInfo

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
