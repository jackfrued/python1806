"""
自定义接口数据过滤器
"""
from django_filters.rest_framework import FilterSet, CharFilter, NumberFilter

from common.models import Estate, HouseInfo


class HouseInfoFilter(FilterSet):
    """自定义房源信息过滤器"""

    title = CharFilter(lookup_expr='icontains')
    min_area = NumberFilter(field_name='area', lookup_expr='gte')
    max_area = NumberFilter(field_name='area', lookup_expr='lte')
    min_price = NumberFilter(field_name='price', lookup_expr='gte')
    max_price = NumberFilter(field_name='price', lookup_expr='lte')

    class Meta:
        model = HouseInfo
        fields = ('title', 'min_area', 'max_area', 'min_price', 'max_price', 'district')


class EstateFilter(FilterSet):
    """自定义楼盘数据过滤器"""

    name = CharFilter(lookup_expr='startswith')

    class Meta:
        model = Estate
        fields = ('name', 'district')
