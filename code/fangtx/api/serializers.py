"""
自定义模型序列化器
"""
from drf_haystack.serializers import HaystackSerializer
from rest_framework import serializers

from api.indexes import HouseInfoIndex
from common.models import District, Estate, Agent, HouseType, HouseInfo, Tag


class DistrictSerializer(serializers.ModelSerializer):
    """地区基本信息序列化器"""

    class Meta:
        model = District
        fields = ('distid', 'name')


class DistrictDetailSerializer(serializers.ModelSerializer):
    """地区详情（带子一级行政区域）序列化器"""

    cities = serializers.SerializerMethodField()

    @staticmethod
    def get_cities(district):
        queryset = District.objects.filter(parent__distid=district.distid)\
            .only('distid', 'name')
        return DistrictSerializer(queryset, many=True).data

    class Meta:
        model = District
        fields = ('distid', 'name', 'intro', 'cities')


class AgentSerializer(serializers.ModelSerializer):
    """地产经理人序列化器"""

    class Meta:
        model = Agent
        fields = ('agentid', 'name', 'tel', 'servstar', 'certificated')


class EstateSerializer(serializers.ModelSerializer):
    """楼盘详细信息序列化器"""

    district = serializers.SerializerMethodField()
    agents = serializers.SerializerMethodField()

    @staticmethod
    def get_district(estate):
        return DistrictSerializer(estate.district).data

    @staticmethod
    def get_agents(estate):
        return AgentSerializer(estate.agents, many=True).data

    class Meta:
        model = Estate
        fields = '__all__'


class EstateSimpleSerializer(serializers.ModelSerializer):
    """楼盘基本信息序列化器"""

    class Meta:
        model = Estate
        fields = ('estateid', 'name', 'hot')


class HouseTypeSerializer(serializers.ModelSerializer):
    """户型序列化器"""

    class Meta:
        model = HouseType
        fields = '__all__'


class TagSerializer(serializers.ModelSerializer):
    """标签序列化器"""

    class Meta:
        model = Tag
        fields = ('name', )


class HouseInfoSerializer(serializers.ModelSerializer):
    """房源序列化器"""
    price = serializers.SerializerMethodField()
    type = serializers.SerializerMethodField()
    district = serializers.SerializerMethodField()
    estate = serializers.SerializerMethodField()
    agent = serializers.SerializerMethodField()
    tags = serializers.SerializerMethodField()

    @staticmethod
    def get_price(houseinfo):
        return f'{houseinfo.price} {houseinfo.priceunit}'

    @staticmethod
    def get_type(houseinfo):
        return houseinfo.type.name

    @staticmethod
    def get_district(houseinfo):
        return DistrictSerializer(houseinfo.district).data

    @staticmethod
    def get_estate(houseinfo):
        return EstateSimpleSerializer(houseinfo.estate).data

    @staticmethod
    def get_agent(houseinfo):
        return AgentSerializer(houseinfo.agent).data

    @staticmethod
    def get_tags(houseinfo):
        results = []
        for tag in houseinfo.tags.all():
            results.append(tag.name)
        return results

    class Meta:
        model = HouseInfo
        fields = ('houseid', 'title', 'area', 'floor', 'totalfloor',
                  'direction', 'price', 'detail', 'mainphoto', 'pubdate',
                  'street', 'type', 'district', 'estate', 'agent', 'tags')


class HouseInfoIndexSerializer(HaystackSerializer):
    """房源全文检索数据序列化器"""

    object = HouseInfoSerializer(read_only=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    class Meta:
        index_classes = [HouseInfoIndex]
        fields = ('text', 'object')
