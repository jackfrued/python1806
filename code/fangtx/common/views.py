"""
公共部分的视图函数
"""
from django.http import JsonResponse, HttpResponse

from common.captcha import Captcha
from common.utils import gen_mobile_code, send_sms_by_luosimao, gen_captcha_text, gen_qrcode


def mobile_code(request, tel):
    """发送短信验证码"""
    code = gen_mobile_code()
    request.session['mobile_code'] = code
    # 如果调用短信网关的函数返回字典对象就用JsonResponse进行处理
    # 如果返回的是字符串就用HttpResponse并指定MIME类型即可
    # 调用三方平台的一个风险就是时间不可预估 但是我们的应用不能够因为三方平台而阻塞
    # 所以通常调用三方平台而且不需要马上获得执行结果的场景都要进行异步化的处理
    # 让发送短信的函数延迟执行（将函数调用变成一条消息放到消息队列）- 消息的生产者
    send_sms_by_luosimao.delay(tel, code)
    return JsonResponse({'code': 20000, 'message': '短信验证码已经发出'})


def captcha(request):
    """获取图片验证码"""
    code_text = gen_captcha_text()
    request.session['captcha_code'] = code_text
    code_bytes = Captcha.instance().generate(code_text)
    return HttpResponse(code_bytes, content_type='image/png')


def get_qr_code(request):
    url = request.GET.get('url', 'http://jackfrued.xyz')
    return HttpResponse(gen_qrcode(url.encode()), content_type='image/png')
