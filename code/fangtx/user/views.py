"""
用户相关操作的视图函数
"""
from django.shortcuts import render, redirect


def to_login(request):
    """登录页"""
    return render(request, 'login.html', {})


def to_register(request):
    """注册页"""
    return render(request, 'register.html', {})


def to_logout(request):
    """注销"""
    request.session.flush()
    return redirect('/')
